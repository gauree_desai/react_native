import React from 'react'
import { ScrollView, View,Text,Image,StyleSheet } from 'react-native'
import data from '../JSON/native.json'

export default function List() {
    return (
        <ScrollView>
            <View>
              {
                data.restaurants.map((item,key)=>{
                    return (
                        <View key={key}>
                       
                        <Image source={{uri:item.image}} style={styles.placeImage}/>
                        <Text>
                            {item.name}
                        </Text>
                        </View>
                    )

                })
              }
            </View>
       </ScrollView>
    )
}


const styles = StyleSheet.create({
    listContainer:{
        width:"80%"
      },
    listItem:{
        width:"100%",
        padding:10,
        marginTop:2,
        color:'#191970',
        backgroundColor: '#eee',
        alignSelf:'stretch',
    },
    placeImage:{
        marginRight:8,
        height:200
    },
    textval:{
        textAlign:'center',
        fontSize:30
    }
})
